# 
import os
import numpy as np
import xml.etree.ElementTree as ET
import re
import pandas as pd
from functools import reduce
import xmltodict
#
from xbrl import XBRLParser, GAAP, GAAPSerializer

xbrl_parser = XBRLParser()
# %%
file = open('/home/rvelez/Projects/Githubeliot/BMV/xml/ifrsxbrl_918294_2018-04D_1 (1)_xbrl.txt', encoding="ISO-8859-1")
# %%
def recursive_items(dictionary):
    for key, value in dictionary.items():
        if type(value) is dict:
            yield from recursive_items(value)
        else:
            yield (key, value)

def xmltodf(etiqueta):
    claves=[]
    valores=[]
    for key, value in recursive_items(etiqueta):
        claves.append(key)
        valores.append(value)
    dictionary = dict(zip(claves, valores))
    return dictionary
# %%

# %%
def rep(ruta_zip):
    for file in os.listdir(zip_path):
        print(os.listdir(zip_path))
        if re.search('[A-Za-z0-9 ]{1,}\.[xml]{1,3}', file):
            ruta=(zip_path+file)
            print(ruta)
            with open(ruta,) as fl:
                doc = xmltodict.parse(fl.read(),dict_constructor = dict)
            df=pd.DataFrame([xmltodf(y) for y in (x for x in doc['reporte']['operaciones']['operacion'] if ('cliente' in x['persona_reportada'])) if ('persona_fisica' in y['persona_reportada']['cliente']['tipo_persona'])])
            #tclienteM=pd.DataFrame([xmltodf(y) for y in (x for x in doc['reporte']['operaciones']['operacion'] if ('cliente' in x['persona_reportada'])) if ('persona_moral' in y['persona_reportada']['cliente']['tipo_persona'])])
            #tusuarioF=pd.DataFrame([xmltodf(x) for x in doc['reporte']['operaciones']['operacion'] if ('usuario' in x['persona_reportada'])])
            print('Se agrega lista: ' + file)
    print('se creó el dataframe')
    return df
# %%
from xbrl import XBRLParser, GAAP, GAAPSerializer
# %%
file = '/home/rvelez/Documents/GestelBMV/GFAMSA/xml/ifrsxbrl_918294_2018-04D_1 (1)_xbrl.xml'
# %% markdown
# ### Intento Fallido, dice que el XBRL es fallido, voy a intentar leerlo com si fuera un XML normal
# %%
xbrl_parser = XBRLParser()
# %%
xbrl = xbrl_parser.parse(file)
# %%
xbrl_parser = XBRLParser(0)
#file_to_parse = "/home/rvelez/Documents/GestelBMV/GFAMSA/xml/ifrsxbrl_918294_2018-04D_1 (1)_xbrl.xml"
xbrl = xbrl_parser.parse(file)
dei_obj = xbrl_parser.parseDEI(xbrl)
# %% markdown
# ### Otro intento como si fuerea un XML normal
# %%
import os
import numpy as np
import xml.etree.ElementTree as ET
import re
import pandas as pd
from functools import reduce
import xmltodict

from xbrl import XBRLParser, GAAP, GAAPSerializer

xbrl_parser = XBRLParser()

path = '/home/rvelez/Documents/GestelBMV/GFAMSA/xml/ifrsxbrl_918294_2018-04D_1 (1)_xbrl.xml'
#file = open('/home/rvelez/Documents/GestelBMV/GFAMSA/xml/ifrsxbrl_918294_2018-04D_1 (1)_xbrl.xml', encoding="ISO-8859-1")
# %%
list(file)
# %%
#Este no me sive
with open(file) as fl:
    doc = xmltodict.parse(fl.read(),dict_constructor = dict)
# %%
doc = xmltodict.parse(file)
# %% markdown
# ### Intentando leer XBRL
# %%
xbrl_parser = XBRLParser(0)
# %%
file_to_parse = "/home/rvelez/Documents/GestelBMV/GFAMSA/xml/ifrsxbrl_918294_2018-04D_1 (1)_xbrl.xml"
# %%
list(file)
# %%
xbrl = xbrl_parser.parse(file)
# %%
dei_obj = xbrl_parser.parseDEI(xbrl)
# %%
serializer = DEISerializer()
result = serializer.dump(dei_obj)
# %% markdown
# ###  Vamos a leerlo de otra forma
# %% markdown
# #### Con ayuda de la paguina: https://github.com/greedo/python-xbrl/blob/master/examples/gaap.py
# %%

#! /usr/bin/env python
# encoding: utf-8
from __future__ import print_function

from xbrl import XBRLParser, GAAP, GAAPSerializer, DEISerializer


xbrl_parser = XBRLParser(precision=0)
# %%
path = '/home/rvelez/Projects/Githubeliot/BMV/xml/ifrsxbrl_918294_2018-04D_1 (1)_xbrl.txt'
# %%
# Parse an incoming XBRL file
xbrl = xbrl_parser.parse(path)
# %%
f = open("/home/rvelez/Projects/Githubeliot/BMV/xml/ifrsxbrl_918294_2018-04D_1 (1)_xbrl.txt",encoding='iso-8859-15',mode="r")
# %%
%%time
f = f.read()
# %%
f
# %%
from bs4 import BeautifulSoup
# %%
help(BeautifulSoup)
# %%
