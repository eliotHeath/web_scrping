
import os
import pandas as pd
import re
from PyPDF2 import PdfFileReader, PdfFileWriter
import process_image as p_i

import argparse
from AC_READER import *
import pandas as pd
from PyPDF2 import PdfFileReader, PdfFileWriter
#from ID_lect import *
from pprint import pprint
import cv2


#=============================================================================
#=============================================================================
def bin_image(file):
    '''
    Función para la binarización de la imagen
    '''
    #pdf -> tiff
    os.system("convert -density 300 "+file+" -depth 8 -strip -background grey -alpha off "+file[:-4]+".tiff")
    print('\nPDF -> TIFF\n+++++')
    # lectura de la imagen en escala de grises

    #file = file.replace('\ ', ' ')

    image = cv2.imread(file[:-4]+'.tiff', cv2.IMREAD_GRAYSCALE)
    print('\ningreso tiff\n+++++')

    #usando threshold
    ret, im_bw = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY|cv2.THRESH_OTSU)
    print('\nthresh\n+++++')


    #show_res_image(im_bw)
    # escritura del archivo
    cv2.imwrite(file[:-4]+".tiff", im_bw)
    print('\nguardado del tiff\n+++++')

    # fin de la función
    return 'imagen binarizada'
#=============================================================================
#=============================================================================
file = '/home/rvelez/Projects/Githubeliot/Insumos_Bancos/BMV INSUMOS/GFAMSA/dictamen_918581_2018_1.pdf'
save_path = '/home/rvelez/Projects/Githubeliot/Insumos_Bancos/BMV INSUMOS/GFAMSA/TXT'

#Abro el pdf con estado financiero
path = '/home/rvelez/Projects/Githubeliot/BMV/Evidencia/2/page.txt'
f = open (path,'r')
with open(path, "rt", encoding = "utf-8") as f:
    file_content = f.read()

file_content

respuesta_1=re.findall('ABSTENCIÓN', file_content)
respuesta_2=re.findall('OPINION', file_content)
respuesta_3=re.findall('SALVEDAD', file_content)
respuesta_4=re.findall('SIMPLE', file_content)


reader = PdfFileReader(file)
t_pages = reader.getNumPages()
writer = PdfFileWriter()

writer.addPage(reader.getPage(0))
with open(save_path + '/page.pdf', 'wb') as outfile:
            writer.write(outfile)

page = save_path + '/page.pdf'

bin_image(page)
