# -*- coding: utf-8 -*-
"""
Created on Sat Oct 10 22:24:10 2020

@author: Heathcliff
"""

import newspaper
import pandas as pd
import re
import time

import warnings
warnings.filterwarnings("ignore")

'''
Network_Mining:
    |---- url: Ce champ est nécessaire, il doit s'agir d'une url valide pour que le programme puisse extraire les informations.
    |---- paper: Nom du journal.
    |---- path_art: Chemin vers lequel vous souhaitez diriger les résultats.
    |---- path_url: Chemin vers lequel vous souhaitez diriger les résultats. 
    |---- date: Date. Ce champ peut être facultatif.
    
search:
    |---- word: Parole qui vous veux rerechehce dans la information
    |---- Art: C'est la voie de l'information exploitée.
'''

class Networks_Web:

        def __init__(self,paper,data):
            self.paper = paper
            self.data = data

        
        def Network_Mining(url,paper,path,date):

            n_paper = newspaper.build(url)
            art = []
            url = []

            for article in n_paper.articles:
                article.download()
                art.append(article.html)
                print(article.url)
                url.append(article.url)
                print(article.publish_date)
                time.sleep(2)
            Art = pd.DataFrame(art)   
            Url = pd.DataFrame(url)

            #Art.to_csv(path_art+'\\'+'Art_'+paper+'_'+date+'.csv')
            #Url.to_csv(path_url+'\\'+'Url_'+paper+'_'+date+'.csv')
            
            Art.columns = ['Artiuclos']
            Url.columns = ['URL']
            
            con = pd.concat([Art,Url],axis=1)
            
            con.to_csv(path+'\\'+'Datos_'+paper+'_'+date+'.csv')
            
            return con

        def Parole(word,Art):
            for i in range(len(d)):
                for j in range(len(Art[0])):
                    if re.search(word,Art[0][j]):
                        l.append(Art[0][j])
            G = pd.DataFrame(l)
            return G
        
        
        def Bulk_Parole(d,con):
            l = []
            for i,e in enumerate(d[0]):
                for j,n in enumerate(con['Artiuclos']):
                    if re.search(e,n):
                        l.append((con['Artiuclos'][j],con['URL'][j]))
            G = pd.DataFrame(l).rename(columns={0:'Articulos',1:'URL'})
            return G